#!/usr/bin/env python3
import logging
import random
import os.path
import toml
import json

config = 'python_bot.ini'
conf_dic = toml.load(config)

if conf_dic['credentials']['bot_token'] == "BOT:TOKEN":
  print("Please replace BOT:TOKEN by your bot token in python_bot.ini")
  exit

tarot_dic= toml.load(conf_dic['methods']['tarot']+'/method.ini')

from telegram import Update, ForceReply
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

msgext="msg.json"
idext="file_id"
cartes=tarot_dic["tarot"]["cartes"]
murphies=tarot_dic["tarot"]["murphies"]

def tarot(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(cartes[random.randint(0,len(cartes)-1)])

# Define a few command handlers. These usually take the two arguments update and
# context.
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    update.message.reply_markdown_v2(
        fr'Hi {user.mention_markdown_v2()}\!',
        reply_markup=ForceReply(selective=True),
    )

def hello(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(f'Hello {update.effective_user.first_name}')

def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text('Commandes:\ntarot\nmurphy\nhelp')


def echo(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    update.message.reply_text(update.message.text)

def sendmurphy(update: Update, context: CallbackContext) -> None:
####    update.message.reply_sticker(open("tarot/murphy.tgs".format(user_id), 'rb'))
    stickfile="tarot/tgs/"+murphies[random.randint(0,len(murphies)-1)]
    print('stickfile:'+stickfile)
    jsfile=stickfile+'.'+msgext
    idfile=stickfile+'.'+idext
    if os.path.exists(idfile):
      print(idfile+" exists")
      fid=open(idfile)
      stickid=fid.read()
      fid.close()
      print("stickid from idfile="+stickid)
      msg = update.message.reply_sticker(stickid)
      print("upload by ID ok")
      print(msg)
    else:
      print(idfile+" doesn't exists")
      msg = update.message.reply_sticker(open(stickfile, 'rb'))
      print(msg)
      print("create idfile "+idfile)
      fid=open(idfile, 'w')
      fid.write(msg['sticker']['file_id'])
      fid.close()

def main() -> None:
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(conf_dic['credentials']['bot_token'])

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler('hello', hello))
    dispatcher.add_handler(CommandHandler('tarot', tarot))
    dispatcher.add_handler(CommandHandler("murphy", sendmurphy))
#    # on non command i.e message - echo the message on Telegram
#    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()
