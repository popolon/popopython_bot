This is a simple telegram bot example based on [python-telegram-bot](https://python-telegram-bot.org/). It can send random message from word tarot, or random TGS (Telegram gzipped and subset of Lottie animation).

If you want to create Lottie (or SVG) animation you can use [python-lottie](https://pypi.org/project/lottie/) for easy procedural generation or [Glaxnimate](https://glaxnimate.mattbas.org/), I used for the current .tgs example, a GUI to produce vector animations mainly based on Lottie format. It can still export in animated SVG, mp4, gif, multi-PNG, etc...

To use it:
* Replace "BOT:TOKEN" by token that [Botfather given you](https://core.telegram.org/bots)
* Customize it if you want
* Launch it by ./python_bot.py
A better (but not the best) way is to launch it in a screen:

```screen -S Pythonbot ./python_bot.py```

and to dettach by 'ctrl'-'a' + 'd'
you can come back in the screen by typing
```screen -r```

if you have only this screen, or

```screen -x Pythonbot```

You can only launch one bot at a time with an unique telegram bot id.
